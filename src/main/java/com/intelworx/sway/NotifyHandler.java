/*
 * Copyright 2014 JosephT.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.intelworx.sway;

import java.util.List;
import org.elasticsearch.common.logging.ESLogger;
import twitter4j.JSONArray;
import twitter4j.JSONObject;

/**
 *
 * @author JosephT
 */
public class NotifyHandler {

    //private static final String notifyUrl = "http://ec2-54-186-106-88.us-west-2.compute.amazonaws.com";
    final private static HttpClient client = new HttpClient("sway.intelworx.com", 80);
    final private static String pushPath = "/index.php/tweets/pushtweet";

    static public boolean send(List<JSONObject> objects, ESLogger logger) {
        JSONArray array = new JSONArray(objects);
        HttpClientResponse response = client.request("POST", pushPath, array.toString());
        
        if (logger != null) {
            logger.debug("Response: %s", response.response());
        }
        
        return response.errorCode() == 200;
    }
}
