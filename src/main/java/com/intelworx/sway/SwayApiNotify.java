/*
 * Copyright 2014 JosephT.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.intelworx.sway;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.elasticsearch.common.logging.ESLogger;
import twitter4j.JSONException;
import twitter4j.JSONObject;

/**
 *
 * @author JosephT
 */
public class SwayApiNotify {

    private final ConcurrentLinkedQueue<JSONObject> waitingToPush;
    private long lastPushTime = System.currentTimeMillis();
    private long notifyTimeout;
    private int queueLimit;
    private ESLogger loggger;

    private class PushTimeout extends TimerTask {

        @Override
        public void run() {
            push();
        }
    }

    public SwayApiNotify(long timeout, int limit, ESLogger logger) {
        waitingToPush = new ConcurrentLinkedQueue<JSONObject>();
        queueLimit = limit;
        notifyTimeout = timeout;
        //timer start
        Timer t = new Timer();
        t.scheduleAtFixedRate(new PushTimeout(), 0, timeout);
        this.loggger = logger;
    }

    public long getLastPushTime() {
        return lastPushTime;
    }

    public void setLastPushTime(long lastPushTime) {
        this.lastPushTime = lastPushTime;
    }

    public long getNotifyTimeout() {
        return notifyTimeout;
    }

    public void setNotifyTimeout(long notifyTimeout) {
        this.notifyTimeout = notifyTimeout;
    }

    public int getQueueLimit() {
        return queueLimit;
    }

    public void setQueueLimit(int queueLimit) {
        this.queueLimit = queueLimit;
    }

    public void addQueueToQueue(JSONObject status) {
        waitingToPush.add(status);
        //check should push
        if (waitingToPush.size() >= queueLimit) {
            push();
        }
    }
    
    public void addQueueToQueue(String jsonString) {
        try {
            addQueueToQueue(new JSONObject(jsonString));
        } catch (JSONException ex) {
            Logger.getLogger(SwayApiNotify.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    synchronized private void push() {
        //oush to the server
        if (waitingToPush.isEmpty()) {
            return;
        }

        final ArrayList<JSONObject> statuses = new ArrayList<JSONObject>();
        Iterator<JSONObject> statusIterator = waitingToPush.iterator();
        while (statusIterator.hasNext() && statuses.size() < queueLimit) {
            statuses.add(statusIterator.next());
            statusIterator.remove();
        }
        
        //push to rotimi
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                if(NotifyHandler.send(statuses, loggger)){
                    loggger.info("%d Tweets pushed to web server", statuses.size());
                }else{
                    loggger.warn("An error occured while pushing to server!");
                }
            }
        });
        
        t.start();
    }

}
